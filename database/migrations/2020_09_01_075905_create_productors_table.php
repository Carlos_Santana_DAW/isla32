<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productores', function (Blueprint $table) {
            $table->id();
            $table->string("nombre",255)->nullable();
            $table->set("tipo",["Agroalimentarios","Arte","Artesania","Deportes","Excursiones","Alquiler_vehiculos","Hospedaje_vacacional","Espectaculos","Visitas_guiadas","Musica"])->nullable();
            $table->string("direccion",255)->nullable();
            $table->string("correo",255)->nullable();
            $table->string("telefono",255)->nullable();
            $table->string("web",255)->nullable();
            $table->set("redes_sociales",["Twitter","Linkedin","Facebook","Instagram","Ninguna"])->default("Ninguna")->nullable();
            $table->string("usuario_facebook",255)->nullable();
            $table->string("usuario_twitter",255)->nullable();
            $table->string("usuario_instagram",255)->nullable();
            $table->string("usuario_linkedin",255)->nullable();
            $table->enum("asociado",["Si","No"])->default("No");
            $table->timestamps();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productores');
    }
}
