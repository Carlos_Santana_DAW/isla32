<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;

// Crea el usuario admin con id 1
class InsertAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //usuario admin 
        $admin = new User;
        $admin->id = 1;
        $admin->name = "Admin";
        $admin->email = "informatica@isla32.com";
        $admin->email_verified_at = new DateTime();
        $admin->password = Hash::make("isla123");
        $admin->rol = "ADMIN";
        $admin->save();

        //usuario administracion y finanzas
        $finanza = new User;
        $finanza->id = 2;
        $finanza->name = "AdminFinanzas";
        $finanza->email = "adminfinanzas@isla32.com";
        $finanza->email_verified_at = new DateTime();
        $finanza->password = Hash::make("isla123");
        $finanza->rol = "ADMINFINANZA";
        $finanza->save();

        //usuario marketing
        $marketing = new User;
        $marketing->id = 3;
        $marketing->name = "Marketing";
        $marketing->email = "marketing@isla32.com";
        $marketing->email_verified_at = new DateTime();
        $marketing->password = Hash::make("isla123");
        $marketing->rol = "MARKETING";
        $marketing->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
