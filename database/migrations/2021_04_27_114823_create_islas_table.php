<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIslasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('islas', function (Blueprint $table) {
            $table->id();
            $table->unsignedbiginteger("idarchipielago");
            $table->string("nombre",255);
            $table->integer("codpostal");
            $table->string("provincia",255);
            $table->timestamps();
            $table->foreign("idarchipielago")->references("id")->on("archipielagos")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('islas');
    }
}
