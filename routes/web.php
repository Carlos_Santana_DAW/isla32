<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductosController;
use App\Http\Controllers\ClientesController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductorController;
use App\Http\Controllers\IslaController;
use App\Http\Controllers\ArchipielagoController;
use App\Http\Controllers\MarcaController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(["verify" => "true"]);

Route::group(["middleware" => "verified"], function() {

    Route::get('/',[HomeController::class ,"index"]);

    //rutas de importado y exportado de datos
    Route::get('importExportView', [ProductorController::class,'importExportView']);
    Route::get('export', [ProductorController::class,'export'])->name('export');
    Route::post('import', [ProductorController::class,'import'])->name('import');

    //agrupacion de rutas admin
    Route::group(["middleware" => "admin"], function() {
    
        //rutas usuarios
        Route::get("/usuarios/show", [ClientesController::class, "showClientes"]);
        Route::get("/usuarios/edit/{id}", [ClientesController::class, "getEditClientes"]);
        Route::put("/usuarios/edit/{id}", [ClientesController::class, "putEditClientes"]);
        Route::delete("/usuarios/delete/{id}", [ClientesController::class, "deleteClientes"]);
        Route::get("/usuarios/create",[ClientesController::class, "getCreateCliente"]);
        Route::post("/usuarios/create",[ClientesController::class,"postCreateCliente"]);
        
    });
    
        //rutas productos
        Route::get("/productos/show", [ProductosController::class, "showProductos"]);
        Route::get("/productos/create", [ProductosController::class, "getCreateProductos"]);
        Route::post("/productos/create", [ProductosController::class, "postCreateProductos"]);
        Route::get("/productos/edit/{id}", [ProductosController::class, "getEditProductos"]);
        Route::put("/productos/edit/{id}", [ProductosController::class, "putEditProductos"]);
        //Route::get("/productos/imagen/{id}", [ProductosController::class, "getImagen"]);
        //Route::put("/productos/imagen/{id}", [ProductosController::class, "putImagen"]);
        Route::delete("/productos/delete/{id}", [ProductosController::class, "deleteProductos"]);

        //rutas productores
        Route::get("/productor/show",[ProductorController::class,"showProductor"]);
        Route::get("/productor/redsocial",[ProductorController::class,"showRedSocialProductor"]);
        Route::get("/productor/edit/{id}",[ProductorController::class,"getEditProductor"]);
        Route::put("/productor/edit/{id}",[ProductorController::class,"PutEditProductor"]);
        Route::delete("/productor/delete/{id}",[ProductorController::class,"deleteProductor"]);
        Route::get("/productor/create",[ProductorController::class,"getCreateProductor"]);
        Route::post("/productor/create",[ProductorController::class,"PostCreateProductor"]);
        

        //rutas islas
        Route::get("/islas/show",[IslaController::class,"showIsla"]);
        Route::get("/islas/edit/{id}",[IslaController::class,"getEditIsla"]);
        Route::put("/islas/edit/{id}",[IslaController::class,"PutEditIsla"]);
        Route::delete("/islas/delete/{id}",[IslaController::class,"deleteIsla"]);
        Route::get("/islas/create",[IslaController::class,"getCreateIsla"]);
        Route::post("/islas/create",[IslaController::class,"PostCreateIsla"]);


        //rutas archipielagos
        Route::get("/archipielagos/show",[ArchipielagoController::class,"showArchipielago"]);
        Route::get("/archipielagos/edit/{id}",[ArchipielagoController::class,"getEditArchipielago"]);
        Route::put("/archipielagos/edit/{id}",[ArchipielagoController::class,"PutEditArchipielago"]);
        Route::delete("/archipielagos/delete/{id}",[ArchipielagoController::class,"deleteArchipielago"]);
        Route::get("/archipielagos/create",[ArchipielagoController::class,"getCreateArchipielago"]);
        Route::post("/archipielagos/create",[ArchipielagoController::class,"PostCreateArchipielago"]);


        //rutas marcas
        Route::get("/marcas/show",[MarcaController::class,"showMarca"]);
        Route::get("/marcas/edit/{id}",[MarcaController::class,"getEditMarca"]);
        Route::put("/marcas/edit/{id}",[MarcaController::class,"PutEditMarca"]);
        Route::delete("/marcas/delete/{id}",[MarcaController::class,"deleteMarca"]);
        Route::get("/marcas/create",[MarcaController::class,"getCreateMarca"]);
        Route::post("/marcas/create",[MarcaController::class,"PostCreateMarca"]);
    

});
