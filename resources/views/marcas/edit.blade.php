@extends("layouts.plantilla")

@section('menu-plantilla')
    @include('partials.menu-plantilla2')
@stop

@section("content")
    @if ($errors->any())
        <div class="row justify-content-center">
            <div class="col-sm-7">
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif

    <div class="row" style="margin-top:40px">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">
                    Modificar la información de la marca
                </div>
                
                <div class="card-body" style="padding:30px">
                    <form method="post" enctype="multipart/form-data">
                        @method("PUT")
                        @csrf
                        <div class="form-group">
                            <label for="name">Nombre de la marca</label>
                            <input type="text" name="nombre" id="nombre" class="form-control" value="{{$marca->nombre}}">
                        </div>

                        <br>      

                        <div class="form-group">
                            <label for="name">Epigrafe</label>
                            <input type="text" name="epigrafe" id="epigrafe" class="form-control" value="{{$marca->nombre}}">
                        </div> 
                        <br>
                        <div class="form-group">
                            <label for="name">Productor</label>
                            <input type="text" name="idproductor" id="idproductor" class="form-control" value="{{$marca->idproductor}}">
                        </div>
                        <br>
                    
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                                Modificar marca
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop