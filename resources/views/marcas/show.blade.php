@extends("layouts.plantilla")

@section("menu-plantilla")
    @include("partials.menu-plantilla2")
@stop

@section("content")
    @if(Session::has("correcto"))
        <div class="alert alert-success">{{Session::get("correcto")}}</div>
    @endif

    </form>
     <table class="table">
        <thead>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Epigrafe</th>
            <th scope="col">Productor</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </thead>

        <tbody>
            @foreach($marca as $key => $marcas)
                <tr>
                    <th scope="row">{{ $marcas->id }}</th>
                    <td>{{ $marcas->nombre }}</td>
                    <td>{{ $marcas->epigrafe }}</td>
                    <td>{{ $marcas->idproductor }}</td>
                    <td><a class="btn btn-warning" href="{{url('/marcas/edit/' . $marcas->id)}}"><i data-feather="edit-3"></i></a></td>
                    <form method = "POST" action ="{{url('/marcas/delete/'. $marcas->id)}}" style = "display:inline">
                        @method('DELETE')
                        @csrf
                    <td><button class="btn btn-danger" type="submit" role="button" ><i data-feather="trash"></i></button></td>
                    </form>
                </tr>
            @endforeach
        </tbody>
    </table>
@stop