<nav id="sidebar" class="sidebar js-sidebar">
			<div class="sidebar-content js-simplebar">
				<a class="sidebar-brand" href="{{url('/')}}">
          <span class="align-middle">Isla32</span>
        </a>

				<ul class="sidebar-nav">

				@if($rol == "ADMIN")
					<li class="sidebar-header">
						Usuarios		
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="{{url('/usuarios/show')}}">
              <i class="align-middle" data-feather="user-check"></i> <span class="align-middle">Visualizar usuarios</span>
            </a>
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="{{url('/usuarios/create')}}">
              <i class="align-middle" data-feather="user-plus"></i> <span class="align-middle">Agregar usuarios</span>
            </a>
					</li>
					@endif

					<li class="sidebar-header">
						Productores
					</li>

					<li class="sidebar-item ">
						<a class="sidebar-link" href="{{url('/productor/show')}}">
              				<i class="align-middle" data-feather="users"></i> <span class="align-middle">Visualizar Productor</span>
            			</a>
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="{{url('/productor/create')}}">
              <i class="align-middle" data-feather="user-plus"></i> <span class="align-middle">Agregar Productor</span>
            </a>
					</li>
					
					<li class="sidebar-header">
						Marcas
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="{{url('/marcas/show')}}">
              <i class="align-middle" data-feather="slack"></i> <span class="align-middle">Visualizar Marcas</span>
            </a>
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="{{url('/marcas/create')}}">
              <i class="align-middle" data-feather="plus"></i> <span class="align-middle">Añadir Marcas</span>
            </a>
					</li>				
					<li class="sidebar-header">
						Productos
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="{{url('/productos/show')}}">
             				 <i class="align-middle" data-feather="tag"></i> <span class="align-middle">Visualizar Productos</span>
            			</a>
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="{{url('/productos/create')}}">
              <i class="align-middle" data-feather="plus"></i> <span class="align-middle">Añadir Producto</span>
            </a>
					</li>

					</li>
					
					


				@if($rol=="ADMIN")
					<li class="sidebar-header">
						Islas
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="{{url('/islas/show')}}">
              <i class="align-middle" data-feather="eye"></i> <span class="align-middle">Visualizar Islas</span>
            </a>
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="{{url('/islas/create')}}">
              <i class="align-middle" data-feather="plus"></i> <span class="align-middle">Añadir Isla</span>
            </a>
					</li>

					<li class="sidebar-header">
						Archipielagos
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="{{url('/archipielagos/show')}}">
              <i class="align-middle" data-feather="archive"></i> <span class="align-middle">Visualizar Archipielago</span>
            </a>
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="{{url('/archipielagos/create')}}">
              <i class="align-middle" data-feather="plus"></i> <span class="align-middle">Añadir Archipielago</span>
            </a>
			@endif
					
				</ul>

			</div>
		</nav>