@extends("layouts.plantilla")

@section('menu-plantilla')
    @include('partials.menu-plantilla2')
@stop

@section("content")
    @if ($errors->any())
        <div class="row justify-content-center">
            <div class="col-sm-7">
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif

    <div class="row" style="margin-top:40px">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">
                    Modificar Usuario
                </div>
                
                <div class="card-body" style="padding:30px">
                    <form method="post" enctype="multipart/form-data">
                        @method("PUT")
                        @csrf
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{$usuarios->name}}">
                        </div>

                            <br>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="email" class="form-control" value="{{$usuarios->email}}">
                        </div>

                        <br>

                        <div class="form-group">
                            <label for="rol">Rol de usuario</label>
                            <select id="rol" name="rol" class="form-control">
                                <option >Selecciona un rol para el usuario</option>
                                @foreach($roles as $key => $rol)
                                <option value="{{$rol}}">{{$rol}}</option>
                                @endforeach
                            </select>        
                        </div>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                                Aplicar cambios
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop