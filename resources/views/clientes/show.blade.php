@extends("layouts.plantilla")

@section("menu-plantilla")
    @include("partials.menu-plantilla2")
@stop

@section("content")
    @if(Session::has("correcto"))
        <div class="alert alert-success">{{Session::get("correcto")}}</div>
    @endif
    
    </form>
    <table class="table">
        <thead>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Email</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </thead>

        <tbody>
            @foreach($clientes as $key => $cliente)
                <tr>
                    <th scope="row">{{ $cliente->id }}</th>
                    <td>{{ $cliente->name }}</td>
                    <td>{{ $cliente->email }}</td>
                    <td><a class="btn btn-warning" href="{{url('/usuarios/edit/' . $cliente->id)}}"><i data-feather="edit-3"></i></a></td>
                    <form method = "POST" action ="{{url('/usuarios/delete/'. $cliente->id)}}" style = "display:inline">
                        @method('DELETE')
                        @csrf
                    <td><button class="btn btn-danger" type="submit" role="button" ><i data-feather="trash"></i></button></td>
                    </form>
                </tr>
            @endforeach
        </tbody>
    </table>
@stop