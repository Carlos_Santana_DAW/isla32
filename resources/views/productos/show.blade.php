@extends("layouts.plantilla")


@section("menu-plantilla")
    @include("partials.menu-plantilla2")
@stop


@section("content")
    @if(Session::has("correcto"))
        <div class="alert alert-success">{{Session::get("correcto")}}</div>
    @endif
    
    <table class="table">
        <thead>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Precio</th>
            <th scope="col">Descripción</th>
            <th scope="col">Marca</th>
            <th scope="col"></th>
            <th scope="col"></th>
            <th scope="col"></th>
        </thead>

        <tbody>
            @foreach($productos as $key => $producto)
                <tr>
                    <th scope="row">{{ $producto->id }}</th>
                    <td>{{ $producto->nombre }}</td>
                    <td>{{ $producto->precio }}</td>
                    <td>{{ $producto->descripcion }}</td>
                    <td>{{ $producto->idmarca }}</td>
                    <td><a class="btn btn-warning" href="{{url('productos/edit/' . $producto->id)}}"><i data-feather="edit-3"></i></a></td>
                    <form method = "POST" action ="{{url('/productos/delete/'. $producto->id)}}" style = "display:inline">
                        @method('DELETE')
                        @csrf
                    <td><button class="btn btn-danger" type="submit" role="button" ><i data-feather="trash"></i></button></td>
                    </form>     
                </tr>
            @endforeach
        </tbody>
    </table>
@stop