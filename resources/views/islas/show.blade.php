@extends("layouts.plantilla")

@section("menu-plantilla")
    @include("partials.menu-plantilla2")
@stop

@section("content")
    @if(Session::has("correcto"))
        <div class="alert alert-success">{{Session::get("correcto")}}</div>
    @endif

    </form>
    <table class="table">
        <thead>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">CodPostal</th>
            <th scope="col">Archipielago</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </thead>

        <tbody>
            @foreach($isla as $key => $islas)
                <tr>
                    <th scope="row">{{ $islas->id }}</th>
                    <td>{{ $islas->nombre }}</td>
                    <td>{{ $islas->codpostal }}</td>
                    <td>{{ $islas->idarchipielago }}</td>
                    <td><a class="btn btn-warning" href="{{url('/islas/edit/' . $islas->id)}}"><i data-feather="edit-3"></i></a></td>
                    <form method = "POST" action ="{{url('/islas/delete/'. $islas->id)}}" style = "display:inline">
                        @method('DELETE')
                        @csrf
                    <td><button class="btn btn-danger" type="submit" role="button" ><i data-feather="trash"></i></button></td>
                    </form>
                </tr>
            @endforeach
        </tbody>
    </table>
@stop