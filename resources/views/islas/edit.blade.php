@extends("layouts.plantilla")

@section('menu-plantilla')
    @include('partials.menu-plantilla2')
@stop

@section("content")
    @if ($errors->any())
        <div class="row justify-content-center">
            <div class="col-sm-7">
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif

    <div class="row" style="margin-top:40px">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">
                    Modificar la información de la isla
                </div>
                
                <div class="card-body" style="padding:30px">
                    <form method="post" enctype="multipart/form-data">
                        @method("PUT")
                        @csrf
                        <div class="form-group">
                            <label for="name">Nombre de la isla</label>
                            <input type="text" name="nombre" id="nombre" class="form-control" value="{{$isla->nombre}}">
                        </div>

                        <br>

                        <div class="form-group">
                            <label for="email">Código postal de la isla</label>
                            <input type="text" name="codpostal" id="codpostal" class="form-control" value="{{$isla->codpostal}}">
                        </div>

                        <br>

                        <div class="form-group">
                            <label for="password">Archipielago al que pertenece</label>
                            <input type="text" name="archipielago" id="archipielago" class="form-control" value="{{$isla->idarchipielago}}">
                        </div>

                        <br>        
                    
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                                Modifica la isla
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop