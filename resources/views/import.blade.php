@extends("layouts.plantilla")

@section('menu-plantilla')
    @include('partials.menu-plantilla2')
@stop

@section('content')
<div class="container">

  <div class="flex-center position-ref full-height">
     
    <div class="container mt-5">
        <h3>Importar productores</h3>
 
        @if ( $errors->any() )
 
            <div class="alert alert-danger">
                @foreach( $errors->all() as $error )<li>{{ $error }}</li>@endforeach
            </div>
        @endif
 
        @if(isset($numRows))
            <div class="alert alert-sucess">
                Se importaron {{$numRows}} registros.
            </div>
            <br>
            <br>
        @endif
 
        <form action="{{route('import')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-3">
                    <div class="custom-file">
                        <input type="file" name="productores" class="custom-file-input" id="productores">
                        <label class="custom-file-label" for="productores">Seleccionar archivo</label>
                    </div>
                    <br>
                    <div class="mt-3">
                        <button type="submit" class="btn btn-primary">Importar</button>
                    </div>
                 </div>
              </div> 
          </form>
        </div>
     </div>
  </div>
@stop