@extends('layouts.app')


@section('content')
 <main class="d-flex w-100">
    <div class="container d-flex flex-column">
        <div class="row vh-100">
            <div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                <div class="d-table-cell align-middle">

                    <div class="text-center mt-4">
                        <h1 class="h2">Bienvenido de vuelta </h1>
                        <p class="lead">
                            Identificate para continuar
                        </p>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="m-sm-4">
                                <div class="text-center">
                                    <img src="{{asset('images/logoisla.png')}}" alt="Logo isla32" class="img-fluid rounded-circle" width="132" height="132" />
                                    <h2>Isla32</h2>
                                </div>
                                <form method="POST" action="{{route('login')}}">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="email" class="form-label">{{ __('Correo') }}</label>
										<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

										@error('email')
                                    		<span class="invalid-feedback" role="alert">
                                        		<strong>{{ $message }}</strong>
                                    		</span>
                                		@enderror

                                    </div>
                                    <div class="mb-3">
                                        <label for="password" class="form-label">{{__('Contraseña')}}</label>
										<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

										@error('password')
                                   		 	<span class="invalid-feedback" role="alert">
                                       			 <strong>{{ $message }}</strong>
                                   			</span>
                            			 @enderror

                                        <small>
										@if (Route::has('password.request'))
        								 <a href="{{ route('password.request') }}">{{__('Recuperar mi contraseña') }}</a>
										@endif

          								</small>
                                    </div>
                                    <div class="text-center mt-3">
                                    <button type="submit" class="btn btn-lg btn-primary">Entrar</button> 
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
   </main>
   @stop