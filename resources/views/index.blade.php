@extends("layouts.plantilla")

@section("menu-plantilla")
    @include("partials.menu-plantilla2")
@stop


@section("content")
<!-- Page Content -->
<div class="container" >
  @if ($errors->any())
    <div class="row justify-content-center" style="margin-top:40px">
        <div class="col-sm-7">
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
  @endif
  
  @if(Session::has("correcto"))
      <div class="alert alert-success">{{Session::get("correcto")}}</div>
  @endif

  <div class="container-fluid p-0">

<h1 class="h3 mb-3"><strong>Analisis</strong> de productores</h1>

<div class="row">
  <div class="col-xl-6 col-xxl-5 d-flex">
    <div class="w-100">
      <div class="row" >
        <div class="col-sm-6">
          <div class="card">
            <div class="card-body">
              <div class="right-col">
                <div class="col mt-2">
                  <h5 class="card-title">Productores Artesanos</h5>
                </div>

                <div class="col-auto">
                  <div class="stat text-primary">
                    <i class="align-middle" data-feather="users"></i>
                  </div>
                </div>
              </div>
              <h1 class="mt-1 mb-3">{{$cuentartesano ?? ''}}</h1>
              
            </div>
          </div>
          <div class="card">
            <div class="card-body">
              <div class="right-col">
                <div class="col mt-2">
                  <h5 class="card-title">Productores de Arte</h5>
                </div>

                <div class="col-auto">
                  <div class="stat text-primary">
                    <i class="align-middle" data-feather="monitor"></i>
                  </div>
                </div>
              </div>
              <h1 class="mt-1 mb-3">{{$cuentarte ?? ''}}</h1>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col mt-1">
                  <h5 class="card-title">Productores Agroalimentarios</h5>
                </div>

                <div class="col-auto">
                  <div class="stat text-primary">
                    <i class="align-middle" data-feather="activity"></i>
                  </div>
                </div>
              </div>
              <h1 class="mt-1 mb-1">{{$cuentalimento ?? ''}}</h1>
              
            </div>
          </div>
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col mt-0">
                  <h5 class="card-title">Productores Deportivos</h5>
                </div>

                <div class="col-auto">
                  <div class="stat text-primary">
                    <i class="align-middle" data-feather="heart"></i>
                  </div>
                </div>
              </div>
              <h1 class="mt-1 mb-5">{{$cuentadeporte ?? ''}}</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-6 col-xxl-7">
    <div class="card flex-fill w-100">
      <div class="card-header">

        <h5 class="card-title mb-0">Productores por epígrafes</h5>
      </div>
      <div class="card-body py-3">
        <div class="chart chart-sm">
          <canvas id="chartjs-dashboard-line"></canvas>
        </div>
      </div>
    </div>
  </div>
</div>
  <!-- gráfico de productores potenciales-->

<div class="row">
  <div class="col-12 col-md-6 col-xxl-3 d-flex order-2 order-xxl-3">
    <div class="card flex-fill w-100">
      <div class="card-header">

        <h5 class="card-title mb-0">Productores asociados</h5>
      </div>
      <div class="card-body d-flex">
        <div class="align-self-center w-100">
          <div class="py-4">
            <div class="chart chart-xs">
              <canvas id="chartjs-dashboard-pie"></canvas>
            </div>
          </div>

          <table class="table mb-0">
            <tbody>
              <tr>
                <td> Potenciales</td>
                <td class="text-end">{{$cuentaasociado}}</td>
              </tr>
              <tr>
                <td>No potenciales</td>
                <td class="text-end">{{$cuentanoasociado}}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-md-6 col-xxl-3 d-flex order-2 order-xxl-3">
    <div class="card flex-fill w-100">
      <div class="card-header">

        <h5 class="card-title mb-0">Redes sociales más usadas</h5>
      </div>
      <div class="card-body d-flex">
        <div class="align-self-right w-100">
          <div class="py-4">
            <div class="chart chart-xs">
              <canvas id="chartjs-dashboard-pie1"></canvas>
            </div>
          </div>

          <table class="table mb-0">
            <tbody>
              <tr>
                <td>Twitter</td>
                <td class="text-end">{{$cuentatwitter}}</td>
              </tr>
              <tr>
                <td>Facebook</td>
                <td class="text-end">{{$cuentafacebook}}</td>
              </tr>
              <tr>
                <td>Instagram</td>
                <td class="text-end">{{$cuentainstagram}}</td>
              </tr>
              <tr>
                <td>Linkedin</td>
                <td class="text-end">{{$cuentalinkedin}}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>


<!--
<div class="row">
  <div class="col-12 col-lg-8 col-xxl-9 d-flex">
    <div class="card flex-fill">
      <div class="card-header">

        <h5 class="card-title mb-0">Latest Projects</h5>
      </div>
      <table class="table table-hover my-0">
        <thead>
          <tr>
            <th>Name</th>
            <th class="d-none d-xl-table-cell">Start Date</th>
            <th class="d-none d-xl-table-cell">End Date</th>
            <th>Status</th>
            <th class="d-none d-md-table-cell">Assignee</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Project Apollo</td>
            <td class="d-none d-xl-table-cell">01/01/2021</td>
            <td class="d-none d-xl-table-cell">31/06/2021</td>
            <td><span class="badge bg-success">Done</span></td>
            <td class="d-none d-md-table-cell">Vanessa Tucker</td>
          </tr>
          <tr>
            <td>Project Fireball</td>
            <td class="d-none d-xl-table-cell">01/01/2021</td>
            <td class="d-none d-xl-table-cell">31/06/2021</td>
            <td><span class="badge bg-danger">Cancelled</span></td>
            <td class="d-none d-md-table-cell">William Harris</td>
          </tr>
          <tr>
            <td>Project Hades</td>
            <td class="d-none d-xl-table-cell">01/01/2021</td>
            <td class="d-none d-xl-table-cell">31/06/2021</td>
            <td><span class="badge bg-success">Done</span></td>
            <td class="d-none d-md-table-cell">Sharon Lessman</td>
          </tr>
          <tr>
            <td>Project Nitro</td>
            <td class="d-none d-xl-table-cell">01/01/2021</td>
            <td class="d-none d-xl-table-cell">31/06/2021</td>
            <td><span class="badge bg-warning">In progress</span></td>
            <td class="d-none d-md-table-cell">Vanessa Tucker</td>
          </tr>
          <tr>
            <td>Project Phoenix</td>
            <td class="d-none d-xl-table-cell">01/01/2021</td>
            <td class="d-none d-xl-table-cell">31/06/2021</td>
            <td><span class="badge bg-success">Done</span></td>
            <td class="d-none d-md-table-cell">William Harris</td>
          </tr>
          <tr>
            <td>Project X</td>
            <td class="d-none d-xl-table-cell">01/01/2021</td>
            <td class="d-none d-xl-table-cell">31/06/2021</td>
            <td><span class="badge bg-success">Done</span></td>
            <td class="d-none d-md-table-cell">Sharon Lessman</td>
          </tr>
          <tr>
            <td>Project Romeo</td>
            <td class="d-none d-xl-table-cell">01/01/2021</td>
            <td class="d-none d-xl-table-cell">31/06/2021</td>
            <td><span class="badge bg-success">Done</span></td>
            <td class="d-none d-md-table-cell">Christina Mason</td>
          </tr>
          <tr>
            <td>Project Wombat</td>
            <td class="d-none d-xl-table-cell">01/01/2021</td>
            <td class="d-none d-xl-table-cell">31/06/2021</td>
            <td><span class="badge bg-warning">In progress</span></td>
            <td class="d-none d-md-table-cell">William Harris</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
-->
  <!--
    calendario de ventas
    <div class="col-12 col-lg-4 col-xxl-3 d-flex">
    <div class="card flex-fill w-100">
      <div class="card-header">

        <h5 class="card-title mb-0">Monthly Sales</h5>
      </div>
      <div class="card-body d-flex w-100">
        <div class="align-self-center chart chart-lg">
          <canvas id="chartjs-dashboard-bar"></canvas>
        </div>
      </div>
    </div>
  </div>
-->
 </div>
</div>
@stop

@section('footer')
  @include('partials.footer-plantilla')
@stop