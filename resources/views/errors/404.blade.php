@extends('layouts.plantilla')

@section('menu-plantilla')
    @include('partials.menu-plantilla2')
@stop


@section('content')
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center text-center">
              <h1 class="error-number">404</h1>
              <h2>Lo sentimos, pero no se ha podido encontrar la página</h2>
              <p>La página que buscas no existe, ¿Desea volver al inicio? 
                </br>
                </br>
                  <button class="btn btn-light"><a href="/">volver al inicio</a></button>
              </p>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
    @endsection
</html>
