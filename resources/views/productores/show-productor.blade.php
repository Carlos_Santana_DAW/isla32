@extends("layouts.plantilla")

@section("menu-plantilla")
    @include("partials.menu-plantilla2")
@stop


@section("content")
    @if(Session::has("correcto"))
        <div class="alert alert-success">{{Session::get("correcto")}}</div>
    @endif
    <!--<form class="form-inline" action="{{ url('/productor/show') }}" method="GET">
    <input type="search" class="form-control" name="texto" id="texto" placeholder="Busqueda" value="{{ $texto ?? '' }}">
    </form>
    
    <button class="btn btn-primary" type="submit">Buscar</button>
    
    -->
    <table class="table">
        <thead>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Tipo</th>
            <th scope="col">Correo</th>
            <th scope="col">Telefono</th>
            <th scope="col">Dirección web</th>
            <th scope="col">Asociado</th>


            <th scope="col"></th>
            <th scope="col"></th>
            <th scope="col"></th>
        </thead>

        <tbody>
        <br>
        <br>
        <br>
        {{$productor = DB::table("productores")->simplePaginate(10)}}
        <br>
        <br>

            @foreach($productor as $key => $productores)
                <tr>
                    <td>{{ $productores->id }}</td>
                    <td>{{ $productores->nombre }}</td>
                    <td>{{ $productores->tipo }}</td>
                    <td>{{ $productores->correo }}</td>
                    <td>{{ $productores->telefono }}</td>
                    <td>{{ $productores->web }}</td>
                    <td>{{ $productores->asociado }}</td>

                    <td><a class="btn btn-warning" href="{{url('/productor/edit/' . $productores->id)}}"><span data-feather="edit-3"></span></a></td>
                    <form method = "POST" action ="{{url('/productor/delete/'. $productores->id)}}" style = "display:inline">
                        @method('DELETE')
                        @csrf
                    <td><button class="btn btn-danger" type="submit" role="button" ><i data-feather="trash"></i></button></td>
                    </form>                    
                    <td><a class="btn btn-info" href="{{url('/productor/redsocial')}}"><span data-feather="chevrons-right"></span></a></td>
                </tr>
            @endforeach
        </tbody>     
    </table>

    <a href="{{url('importExportView')}}"><button class="btn btn-primary" role="button" > importar datos</button></a>
    <br>
    <br>
    <div class ="exportacion">
    <form class="form-inline" action="{{ route('export') }}" method="GET">
    <button class="btn btn-warning" type="submit">exportar datos</button>
    </form>
    </div>
    <br>
@stop