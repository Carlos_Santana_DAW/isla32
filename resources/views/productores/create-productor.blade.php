@extends("layouts.plantilla")

@section("menu-plantilla")
    @include("partials.menu-plantilla2")
@stop

@section("content")
    @if ($errors->any())
    <div class="row justify-content-center" style="margin-top:40px">
        <div class="col-sm-7">
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @endif

    <div class="row" style="margin-top:40px">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">
                    Añadir productor
                </div>
                
                <div class="card-body" style="padding:30px">
                    <form method="post" enctype="multipart/form-data">
                        @method("POST")
                        @csrf
                        <div class="form-group">
                            <label for="nombre">Nombre del productor</label>
                            <input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre')}}">
                        </div>
                        <br>
                        <div class="form-group">
                        <label for="tipo">Tipo de productor</label>
                        <br>
                        <br>
                        @foreach($tipo as $key => $tipos)
                                <input class="form-check-input" type="checkbox" id="tipo" name="tipo" value="{{$tipos}}">
                                    <span class="form-check-label">
                                        {{$tipos}}
                                    </span> 
                                    <br>                               
                        @endforeach 
                        </div>
                           
                        <br>
                        <div class="form-group">
                            <label for="direccion">Direccion</label>
                            <input type="text" name="direccion" id="direccion" class="form-control" value="{{old('direccion')}}">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="correo">Correo Electrónico</label>
                            <input type="email" name="correo" id="correo" class="form-control" value="{{old('correo')}}">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="telefono">Número de telefono</label>
                            <input type="text" name="telefono" id="telefono" class="form-control" value="{{old('telefono')}}">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="web">Dirección Web</label>
                            <input type="text" name="web" id="web" class="form-control" value="{{old('web')}}">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="redes_sociales">Redes Sociales</label>
                            <br>    <br>
                                @foreach($redsocial as $key => $social)
                                <input class="form-check-input" type="checkbox" name="redes_sociales" id="redes_sociales" value="{{$social}}">
                                    <span class="form-check-label">
                                        {{$social}}
                                    </span> 
                                    <br>                                  
                                @endforeach
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="usuario_facebook">Usuario De Facebook</label>
                            <input type="text" name="usuario_facebook" id="usuario_facebook" class="form-control" value="{{old('usuario_facebook')}}">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="usuario_twitter">Usuario de Twitter</label>
                            <input type="text" name="usuario_twitter" id="usuario_twitter" class="form-control" value="{{old('usuario_twitter')}}">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="usuario_instagram">Usuario de instagram</label>
                            <input type="text" name="usuario_instagram" id="usuario_instagram" class="form-control" value="{{old('usuario_instagram')}}">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="usuario_linkedin">Usuario de linkedin</label>
                            <input type="text" name="usuario_linkedin" id="usuario_linkedin" class="form-control" value="{{old('usuario_linkedin')}}">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="asociado">¿Eres Asociado?</label>
                            <select name ="asociado" id ="asociado">
                                @foreach($asociado as $key => $socio)
                                <option value="{{$socio}}">{{$socio}}</option>
                                @endforeach
                            </select>
                        </div>
                        
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                                Añadir productor
                            </button>
                        </div>
                    </form>
               </div>
            </div>
        </div>
    </div>
@stop