@extends("layouts.plantilla")

@section("menu-plantilla")
    @include("partials.menu-plantilla2")
@stop

@section("content")
    @if(Session::has("correcto"))
        <div class="alert alert-success">{{Session::get("correcto")}}</div>
    @endif
    
    <table class="table">
        <thead>
            <th scope="col">Nombre Productor</th>
            <th scope="col">Red Social</th>
            <th scope="col">Usuario de Facebook</th>
            <th scope="col">Usuario de Twitter</th>
            <th scope="col">Usuario de Instagram</th>
            <th scope="col">Usuario de Linkedin</th>

            <th scope="col"></th>
            <th scope="col"></th>
        </thead>

        <tbody>
        <!--paginado de productores-->
        
        {{$productores = DB::table("productores")->simplePaginate(10)}}

            @foreach($productores as $key => $productor)
                <tr>
                    <td>{{ $productor->nombre }}</td>
                    <td>{{ $productor->redes_sociales }}</td>
                    <td>{{ $productor->usuario_facebook }}</td>
                    <td>{{ $productor->usuario_twitter }}</td>
                    <td>{{ $productor->usuario_instagram }}</td>
                    <td>{{ $productor->usuario_linkedin}}</td>

                    <td><a class="btn btn-warning" href="{{url('/productor/edit/' . $productor->id)}}"><span data-feather="edit-3"></span></a></td>
                    <td><a class="btn btn-info" href="{{url('/productor/show')}}"><span data-feather="chevrons-left"></span></a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
@stop