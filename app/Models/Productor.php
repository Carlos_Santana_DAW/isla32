<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productor extends Model
{

    protected $fillable = [
        'nombre',
        'tipo',
        'direccion',
        'correo',
        'telefono',
        'web',
        'redes_sociales',
        'usuario_facebook',
        'usuario_twitter',
        'usuario_instagram',
        'usuario_linkedin',
        'asociado'
    ];

    public $table='productores';
    use HasFactory;

    //Relación 1:N con Islas
    public function islas() {
        return $this->hasMany("\App\Models\Isla");
    }
  
}
