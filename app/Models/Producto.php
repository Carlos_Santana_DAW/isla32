<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    // Relacion 1:N con Marcas
    public function marcas() {
        return $this->hasMany("\App\Models\Marca");
    }
}
