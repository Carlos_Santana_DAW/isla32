<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Isla extends Model
{
    //Relacion 1:N con archipielago
    use HasFactory;
    public function archipielago() {
        return $this->hasMany("\App\Models\Archipielago");
    }
}
