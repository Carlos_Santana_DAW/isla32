<?php

namespace App\Imports;

use App\Models\Productor;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductorImport implements ToModel,WithHeadingRow
{
    private $numRows = 0;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        ++$this->numRows;
        //dd($row);

        //despues de coger la información la vuelca en la base de datos
        return new Productor([
            'nombre' => $row['nombre'],
            'tipo'=> $row['tipo'],
            'direccion'=> $row['direccion'],
            'correo'=> $row['correo'],
            'telefono'=>$row['telefono'],
            'web'=> $row['web'],
            'redes_sociales'=> $row['redes_sociales'],
            'usuario_facebook'=> $row['usuario_facebook'],
            'usuario_twitter'=> $row['usuario_twitter'],
            'usuario_instagram'=> $row['usuario_instagram'],
            'usuario_linkedin'=> $row['usuario_linkedin'],
            'asociado' => $row['asociado'],
        ]);
    }

    public function getRowCount(): int
    {
        return $this->numRows;
    }
}
