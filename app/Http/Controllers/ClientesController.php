<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Http\Requests\ClienteFormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Auth;

// Controlador para cualquier página relacionada con clientes:
class ClientesController extends Controller
{
    // Muestra clientes en el panel de administración
    public function showClientes() {

        if (Auth::check()) {
            $id = Auth::user()->id ?? "NOTHING";
            $usuario = User::findOrFail($id);
            $rol = $usuario->rol;
            $user = DB::table("users")
            ->where("id",$id)
            ->first();
        } else {
            $rol = "NOTHING";
        }

        $clientes = User::all();
        return view("clientes.show", compact("clientes","user","rol"));
    }
/*
    // Muestra el historial de pedidos de un cliente
    public function showHistorial() {
        $comandas = DB::table("comandas")
                        ->where("idCliente", Auth::user()->id)
                        ->get();

        return view("clientes.historial", ["comandas" => $comandas]);
    }
    */
/*
    // Muestra los detalles de una comanda dentro del historial
    public function showComanda($id) {
        $productos = DB::table("comandas_productos")
                            ->join("productos", "comandas_productos.idProducto", "=", "productos.id")
                            ->where("idComanda", $id)
                            ->select("comandas_productos.*", "productos.nombre")
                            ->get();
        
        return view("clientes.comanda", ["productos" => $productos]);
    }
    */



        // Muestra el formulario para crear productos
        public function getCreateCliente() {
            
            if (Auth::check()) {
                $id = Auth::user()->id ?? "NOTHING";
                $usuario = User::findOrFail($id);
                $rol = $usuario->rol;
                $user = DB::table("users")
                ->where("id",$id)
                ->first();
            } else {
                $rol = "NOTHING";
            }

            $roles = ["ADMIN", "ADMINFINANZA", "MARKETING"]; // Pasamos todos los roles posibles para el select

            return view("clientes.create",compact('rol','user','roles'));
        }
    
        // Crea un producto a partir del formulario anterior
        public function postCreateCliente(ClienteFormRequest $request) {
            $validator = $request->validated();
            $usuario = new User;
            $usuario->name = $request->input("name");
            //$producto->imagen = asset("storage/" . $request->file("imagen")->hashName()); // URL de la imagen en public/storage
            $usuario->email = $request->input("email");
            $usuario->rol = $request->input("rol");
            $usuario->password = Hash::make($request->input("password"));
            $usuario->save();
    
            $request->session()->flash("correcto", "Se ha creado el usuario");
    
            return redirect("/usuarios/show");
            
        }




    // Muestra el formulario para editar un cliente
    public function getEditClientes($id) {

        if (Auth::check()) {
            $id1 = Auth::user()->id ?? "NOTHING";
            $usuario = User::findOrFail($id1);
            $rol = $usuario->rol;
            $user = DB::table("users")
            ->where("id",$id1)
            ->first();

        } else {
            $rol = "NOTHING";
        }
        $roles = ["ADMIN", "ADMINFINANZA", "MARKETING"]; // Pasamos todos los roles posibles para el select
        
        

        $usuarios = User::findOrFail($id);
        return view("clientes.edit", compact("usuarios","user","roles","rol"));
    }

    // Edita un cliente según el formulario anterior
    public function putEditClientes($id, ClienteFormRequest $request) {

        if (Auth::check()) {
            $id1 = Auth::user()->id ?? "NOTHING";
            $usuario = User::findOrFail($id1);
            $rol = $usuario->rol;
            $user = DB::table("users")
            ->where("id",$id1)
            ->first();
        } else {
            $rol = "NOTHING";
        }

        // Validación
        $validator = $request->validated();

        // Sustituimos las propiedades del cliente por los inputs
        $cliente = User::findOrFail($id);
        $cliente->name = $request->input("name");
        $cliente->email = $request->input("email");
        $cliente->rol = $request->input("rol");
        $cliente->save();

        // Mensaje de éxito y redirección
        $request->session()->flash("correcto", "Se ha editado el cliente");
        return redirect("/usuarios/show");
    }

    // Elimina un cliente
    public function deleteClientes($id, Request $request) {
        $cliente = User::findOrFail($id);
        $cliente->delete();

        $request->session()->flash("correcto", "Se ha borrado el cliente");
        return redirect("/usuarios/show");
    }

    public function getDeleteClientes() {
        return view("clientes.delete");
    
    }
}
