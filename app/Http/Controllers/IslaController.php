<?php

namespace App\Http\Controllers;
use Auth;
use App\Models\Isla;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\IslaFormRequest;


class IslaController extends Controller
{
    //show para islas
    public function showIsla(Request $request){
         // Guardamos el rol para saber qué botones mostrar en el navegador
         if (Auth::check()) {
            $id = Auth::user()->id ?? "NOTHING";
            $usuario = User::findOrFail($id);
            $rol = $usuario->rol;
            $user = DB::table("users")
            ->where("id",$id)
            ->first();
        } else {
            $rol = "NOTHING";
        }

        $texto = trim($request->get("texto")); // Guardamos la búsqueda
        // Mostramos los productos según la búsqueda y paginamos ocho por página
        $islas = DB::table("islas")
                        ->where("nombre", "LIKE", "%" . $texto . "%")
                        ->paginate(8);

        $isla = Isla::all();

        return view('islas.show',compact("islas","isla","texto","rol","user"));
    }

    //obtener el create para las islas
    public function getCreateIsla() {
        if (Auth::check()) {
            $id = Auth::user()->id ?? "NOTHING";
            $usuario = User::findOrFail($id);
            $rol = $usuario->rol;
            $user = DB::table("users")
             ->where("id",$id)
             ->first();
        } else {
            $rol = "NOTHING";
        }

        return view("islas.create",compact("rol","user"));
    }

    // post del create para las islas
    public function postCreateIsla(IslaFormRequest $request){
        //validación
        $validator = $request->validated();
        
        //inserción de datos
        $islas = new Isla;
        $islas->idarchipielago = $request->input("archipielago");
        $islas->nombre = $request->input("nombre");
        $islas->codpostal = $request->input("codpostal");
        $islas->save();

        $request->session()->flash("correcto", "Se ha creado la isla");
        return redirect("/islas/show");
    }

        // Obtener la vista editar de islas
        public function getEditIsla($id) {

            if (Auth::check()) {
                $id1 = Auth::user()->id ?? "NOTHING";
                $usuario = User::findOrFail($id1);
                $rol = $usuario->rol;
                $user = DB::table("users")
                 ->where("id",$id1)
                 ->first();
            } else {
                $rol = "NOTHING";
            }

            $isla = Isla::findOrFail($id);
            return view("islas.edit", compact("isla","user","rol"));
        }

        //Edita un producto según el formulario anterior
        public function putEditIsla($id,IslaFormRequest $request) {
        //validacion de formulario
        $validator = $request->validated();

        //busqueda para la actualización de los datos de las islas
        $islas = Isla::findOrFail($id);
        $islas->nombre = $request->input("nombre");
        $islas->codpostal = $request->input("codpostal");

        $islas->save();
        
        //mensaje flash que corrobora que se ha editado bien el contenido de los campos
        $request->session()->flash("correcto", "Se ha editado la isla");
        return redirect("/islas/show");
        
       
    }

    // Elimina una isla
    public function deleteIsla($id, Request $request) {
        $isla = Isla::findOrFail($id);
        $isla->delete();

        $request->session()->flash("correcto", "Se ha borrado la isla");
        return redirect("/islas/show");
    }

}
