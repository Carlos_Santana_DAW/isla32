<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Models\Productor;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Imports\ProductorImport;
use App\Exports\ProductorExport;
use App\Http\Requests\ProductorFormRequest;
use Maatwebsite\Excel\Facades\Excel;

class ProductorController extends Controller
{
    //show para productores
    public function showProductor(Request $request){

         // Guardamos el rol para saber qué botones mostrar en el navegador
         if (Auth::check()) {
            $id = Auth::user()->id ?? "NOTHING";
            $usuario = User::findOrFail($id);
            $rol = $usuario->rol;
            $user = DB::table("users")
            ->where("id",$id)
            ->first();

        } else {
            $rol = "NOTHING";
        }

        $texto = trim($request->get("texto")); // Guardamos la búsqueda

        // Mostramos los productores según la búsqueda y paginamos ocho por página

         //$productores = DB::table("productores")
                //->where("nombre", "LIKE", "%" . $texto . "%")
                //->paginate(8);

        //obtener todos los datos

        $productor = Productor::all();

        $paginado = DB::table("productores")->simplePaginate(10);

        return view('productores.show-productor',compact("productor","rol","user","paginado"));
        
    }
    //show para redes sociales de los productores
    public function showRedSocialProductor(){
        if (Auth::check()) {
            $id = Auth::user()->id ?? "NOTHING";
            $usuario = User::findOrFail($id);
            $rol = $usuario->rol;
            $user = DB::table("users")
            ->where("id",$id)
            ->first();
        } else {
            $rol = "NOTHING";
        }

        $productores = Productor::all();
        return view('productores.show-redsocial',compact("user","rol","productores"));
    }

    //obtener el create para los productores
    public function getCreateProductor() {
        if (Auth::check()) {
                $id = Auth::user()->id ?? "NOTHING";
                $usuario = User::findOrFail($id);
                $rol = $usuario->rol;
                $user = DB::table("users")
                ->where("id",$id)
                ->first();
            } else {
                $rol = "NOTHING";
            }

            $tipo = ["Agroalimentarios","Arte","Artesania","Deportes","Excursiones","Alquiler_vehiculos","Hospedaje_vacacional","Espectaculos","Visitas_guiadas"];
            $redsocial = ["Twitter","Linkedin","Facebook","Instagram","Ninguna"];
            $asociado = ["Si","No"];

        return view("productores.create-productor",compact("rol","user","tipo","redsocial","asociado"));
    }

    // post del create para productores
    public function postCreateProductor(ProductorFormRequest $request){
        //validación
        $validator = $request->validated();
        
        //inserción de datos
        $productores = new Productor;
        $productores->nombre = $request->input("nombre");
        $productores->tipo = $request->input("tipo");
        $productores->direccion = $request->input("direccion");
        $productores->correo = $request->input("correo");
        $productores->web = $request->input("web");
        $productores->telefono= $request->input("telefono"); 
        $productores->redes_sociales = $request->input("redes_sociales");
        $productores->usuario_facebook = $request->input("usuario_facebook");
        $productores->usuario_twitter = $request->input("usuario_twitter");
        $productores->usuario_instagram = $request->input("usuario_instagram");
        $productores->usuario_linkedin = $request->input("usuario_linkedin");
        $productores->asociado = $request->input("asociado");
        $productores->save();

        

        $request->session()->flash("correcto", "Se ha creado el productor");

        return redirect('/productor/show');
    }

        // Obtener la vista editar del productor
        public function getEditProductor($id) {
            if (Auth::check()) {
                $id1 = Auth::user()->id ?? "NOTHING";
                $usuario = User::findOrFail($id1);
                $rol = $usuario->rol;

                $user = DB::table("users")
                ->where("id",$id1)
                ->first();
            } else {
                $rol = "NOTHING";
            }

            $productor = Productor::findOrFail($id);
            
            $tipo = ["Agroalimentarios","Arte","Artesania","Deportes","Excursiones","Alquiler_vehiculos","Hospedaje_vacacional","Espectaculos","Visitas_guiadas"];
            $redsocial = ["Twitter","Linkedin","Facebook","Instagram","Ninguna"];
            $asociado = ["Si","No"];

            return view("productores.edit-productor", compact("rol","user","productor","tipo","redsocial","asociado"));
        }

        //Edita un producto según el formulario anterior
        public function putEditProductor($id,ProductorFormRequest $request) {

        //validacion de formulario
        $validator = $request->validated();

        //busqueda para la actualización del productor
        $productores = Productor::findOrFail($id);

        $productores->nombre = $request->input("nombre");
        $productores->tipo = $request->input("tipo");
        $productores->direccion = $request->input("direccion");
        $productores->correo = $request->input("correo");
        $productores->web = $request->input("web");
        $productores->telefono= $request->input("telefono"); 
        $productores->redes_sociales = $request->input("redes_sociales");
        $productores->usuario_facebook = $request->input("usuario_facebook");
        $productores->usuario_twitter = $request->input("usuario_twitter");
        $productores->usuario_instagram = $request->input("usuario_instagram");
        $productores->usuario_linkedin = $request->input("usuario_linkedin");
        $productores->asociado = $request->input("asociado");
        $productores->save();
        
        //mensaje flash que corrobora que se ha editado bien el productor
        $request->session()->flash("correcto", "Se ha editado el productor");

            return redirect("/productor/show");        
       
    }

    // Elimina un productor
    public function deleteProductor($id, Request $request) {
    
        $productor = Productor::findOrFail($id);
        $productor->delete();

        $request->session()->flash("correcto", "Se ha borrado el productor");

        return redirect("/productor/show");
              
        
    }

    //retorna la vista de importaciones
    public function importExportView(){

        if (Auth::check()) {
            $id = Auth::user()->id ?? "NOTHING";
            $usuario = User::findOrFail($id);
            $rol = $usuario->rol;
            $user = DB::table("users")
            ->where("id",$id)
            ->first();

        } else {
            $rol = "NOTHING";
        }
        
        return view('import',compact("user","rol"));
    }
    
    // exportación de excel
    public function export(){
        return Excel::download(new ProductorExport, 'productores.xlsx');
    }

    // importación desde un excel

    public function import(Request $request){
        if (Auth::check()) {
            $id = Auth::user()->id ?? "NOTHING";
            $usuario = User::findOrFail($id);
            $rol = $usuario->rol;
            $user = DB::table("users")
            ->where("id",$id)
            ->first();

        } else {
            $rol = "NOTHING";
        }

        $import = new ProductorImport;
       // $path1 = $request->file('productores')->store('temp');
      //  $path=storage_path('app').'/'.$path1;

        Excel::import($import , request()->file('productores'));
        
        return view('import',['numRows'=>$import->getRowCount(),"user"=>$user,"rol"=>$rol]);
    }

}
