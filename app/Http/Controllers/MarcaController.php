<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\Marca;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\MarcaFormRequest;

class MarcaController extends Controller
{
      //show para archipielagos
      public function showMarca(Request $request){
        // Guardamos el rol para saber qué botones mostrar en el navegador
        if (Auth::check()) {
           $id = Auth::user()->id ?? "NOTHING";
           $usuario = User::findOrFail($id);
           $rol = $usuario->rol;
           $user = DB::table("users")
            ->where("id",$id)
            ->first();
       } else {
           $rol = "NOTHING";
       }

       
       $marca = Marca::all();
       return view('marcas.show',compact("marca","rol","user"));
   }

   //obtener el create para la marca
   public function getCreateMarca() {
    if (Auth::check()) {
        $id = Auth::user()->id ?? "NOTHING";
        $usuario = User::findOrFail($id);
        $rol = $usuario->rol;
        $user = DB::table("users")
         ->where("id",$id)
         ->first();
    } else {
        $rol = "NOTHING";
    }

       return view("marcas.create",compact("rol","user"));
   }

   // post del create para la marca
   public function postCreateMarca(MarcaFormRequest $request){
       //validación
       $validator = $request->validated();
       
       //inserción de datos
       $marcas = new Marca;
       $marcas->nombre = $request->input("nombre");
       $marcas->epigrafe = $request->input("epigrafe");
       $marcas->idproductor = $request->input("idproductor");
       $marcas->save();

       $request->session()->flash("correcto", "Se ha creado la marca");
       return redirect("/marcas/show");
   }

       // Obtener la vista editar para la marca
       public function getEditMarca($id) {

        if (Auth::check()) {
            $id1 = Auth::user()->id ?? "NOTHING";
            $usuario = User::findOrFail($id1);
            $rol = $usuario->rol;
            $user = DB::table("users")
             ->where("id",$id1)
             ->first();
        } else {
            $rol = "NOTHING";
        }

           $marca = Marca::findOrFail($id);
           return view("marcas.edit", compact("user","rol","marca"));
       }

       //Edita un producto según el formulario anterior
       public function putEditMarca($id,MarcaFormRequest $request) {

       //validacion de formulario
       $validator = $request->validated();

       //busqueda para la actualización de los datos de la marca
       $marcas = Marca::findOrFail($id);
       $marcas->nombre = $request->input("nombre");
       $marcas->epigrafe = $request->input("epigrafe");
       $marcas->idproductor = $request->input("idproductor");
       $marcas->save();
       
       //mensaje flash que corrobora que se ha editado bien el contenido de los campos
       $request->session()->flash("correcto", "Se ha editado la marca");
       return redirect("/marcas/show");
       
      
   }

   // Elimina una isla
   public function deleteMarca($id, Request $request) {
       $marca = Marca::findOrFail($id);
       $marca->delete();

       $request->session()->flash("correcto", "Se ha borrado la marca");
       return redirect("/marcas/show");
   }
}
