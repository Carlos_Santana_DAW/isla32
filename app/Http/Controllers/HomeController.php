<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Productor;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
         // Guardamos el rol para saber qué botones mostrar en el navegador
         if (Auth::check()) {
            $id = Auth::user()->id ?? "NOTHING";
            $usuario = User::findOrFail($id);
            $rol = $usuario->rol;
            $user = DB::table("users")
            ->where("id",$id)
            ->first();

        } else {
            $rol = "NOTHING";
        }
        // asociados 
        $asociados = DB::table("productores")
        ->where('asociado','=','Si')
        ->get();
        $cuentaasociado = $asociados->count();

        //no asociados
        $noasociados = DB::table("productores")
        ->where('asociado','=','No')
        ->get();
        $cuentanoasociado = $noasociados->count();

        //productores agroalimentarios
        $alimento = DB::table("productores")
        ->where('tipo','Agroalimentarios')
        ->get();
        $cuentalimento = $alimento->count();

        //productores de arte
        $arte = DB::table("productores")
        ->where('tipo','Arte')
        ->get();
        $cuentarte = $arte->count();

        //productores artesanos
        $artesano = DB::table("productores")
        ->where('tipo','Artesania')
        ->get();
        $cuentartesano = $artesano->count();

        //productores deportivos
        $deporte = DB::table("productores")
        ->where('tipo','Deportes')
        ->get();
        $cuentadeporte = $deporte->count();

        //productores excursiones
        $excursion = DB::table("productores")
        ->where('tipo','Excursiones')
        ->get();
        $cuentaexcursion = $excursion->count();

        //productores alquiler vacacional
        $alquiler = DB::table("productores")
        ->where('tipo','Alquiler_vacacional')
        ->get();
        $cuentavacacional = $alquiler->count();

        //productores hospedaje vacacional
        $hospedaje = DB::table("productores")
        ->where('tipo','Hospedaje_vacacional')
        ->get();
        $cuentahospedaje = $hospedaje->count();

        //productores espectaculos
        $espectaculo = DB::table("productores")
        ->where('tipo','Espectaculos')
        ->get();
        $cuentaespectaculo = $espectaculo->count();

        //productores visitas guiadas
        $guiada = DB::table("productores")
        ->where('tipo','Visitas_guiadas')
        ->get();
        $cuentaguiada = $guiada->count();

        //productores musica
        $musica = DB::table("productores")
        ->where('tipo','Musica')
        ->get();
        $cuentamusica = $musica->count();

        //productores con twitter
        $twitter = DB::table("productores")
        ->where('redes_sociales','Twitter')
        ->get();
        $cuentatwitter = $twitter->count();

        //productores con facebook
        $facebook = DB::table("productores")
        ->where('redes_sociales','Facebook')
        ->get();
        $cuentafacebook = $facebook->count();

        //productores con Instagram
        $instagram = DB::table("productores")
        ->where('redes_sociales','Instagram')
        ->get();
        $cuentainstagram = $instagram->count();

        //productores con Linkedin
        $linkedin = DB::table("productores")
        ->where('redes_sociales','Linkedin')
        ->get();
        $cuentalinkedin= $linkedin->count();

       

            return view('index', compact("rol","user","cuentaasociado"
            ,"cuentanoasociado","cuentartesano","cuentarte",
            "cuentalimento","cuentadeporte","cuentaexcursion"
            ,"cuentavacacional","cuentahospedaje","cuentaespectaculo"
            ,"cuentaguiada","cuentamusica","cuentatwitter"
            ,"cuentafacebook","cuentainstagram","cuentalinkedin"));   

    }
}
