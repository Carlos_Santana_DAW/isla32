<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Archipielago;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ArchipielagoFormRequest;



class ArchipielagoController extends Controller
{
     //show para archipielagos
     public function showArchipielago(Request $request){
         // Guardamos el rol para saber qué botones mostrar en el navegador
         if (Auth::check()) {
            $id = Auth::user()->id ?? "NOTHING";
            $usuario = User::findOrFail($id);
            $rol = $usuario->rol;
            $user = DB::table("users")
            ->where("id",$id)
            ->first();
        } else {
            $rol = "NOTHING";
        }
        $archipielago = Archipielago::all();

        return view('archipielagos.show',compact("archipielago","rol","user"));
    }

    //obtener el create para los archipielagos
    public function getCreateArchipielago() {
        if (Auth::check()) {
            $id = Auth::user()->id ?? "NOTHING";
            $usuario = User::findOrFail($id);
            $rol = $usuario->rol;
            $user = DB::table("users")
             ->where("id",$id)
             ->first();
        } else {
            $rol = "NOTHING";
        }

        return view("archipielagos.create",compact("rol","user"));
    }

    // post del create para el archipielago
    public function postCreateArchipielago(ArchipielagoFormRequest $request){
        //validación
        $validator = $request->validated();
        
        //inserción de datos
        $archipielagos = new Archipielago;
        $archipielagos->nombre = $request->input("nombre");
        $archipielagos->save();

        $request->session()->flash("correcto", "Se ha creado el archipielago");
        return redirect("/archipielagos/show");
    }

        // Obtener la vista editar para el archipielago
        public function getEditArchipielago($id) {
            if (Auth::check()) {
                $id1 = Auth::user()->id ?? "NOTHING";
                $usuario = User::findOrFail($id1);
                $rol = $usuario->rol;
                $user = DB::table("users")
                 ->where("id",$id1)
                 ->first();
            } else {
                $rol = "NOTHING";
            }

            $archipielago = Archipielago::findOrFail($id);
            return view("archipielagos.edit",compact("archipielago","rol","user"));
        }

        //Edita un producto según el formulario anterior
        public function putEditArchipielago($id,ArchipielagoFormRequest $request) {
        //validacion de formulario
        $validator = $request->validated();

        //busqueda para la actualización de los datos de las islas
        $archipielagos = Archipielago::findOrFail($id);
        $archipielagos->nombre = $request->input("nombre");
        $archipielagos->save();
        
        //mensaje flash que corrobora que se ha editado bien el contenido de los campos
        $request->session()->flash("correcto", "Se ha editado el archipielago");
        return redirect("/archipielagos/show");
        
       
    }

    // Elimina una isla
    public function deleteArchipielago($id, Request $request) {
        $archipielago = Archipielago::findOrFail($id);
        $archipielago->delete();

        $request->session()->flash("correcto", "Se ha borrado el archipielago");
        return redirect("/archipielagos/show");
    }
}
