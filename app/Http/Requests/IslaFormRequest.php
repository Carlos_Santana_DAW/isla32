<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IslaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nombre" => "required|string|max:255",
            "codpostal" => "required|digits:5",
            "archipielago"=> "required"
        ];
    }

    public function messages()
    {
        return [
            "nombre.required" => "Se requiere el nombre de la isla",
            "codpostal.required" => "Se requiere el código postal",
            "archipielago.required" => "Se requiere la id del archipielago al que referencia"
        ];
    }

    public function attributes(){
        return[
            "nombre" => "Nombre de la isla",
            "codpostal" => "Código postal de la isla",
            "archipielago" => "Indice del archipielago al que pertenece la isla"
        ];
    }
}
