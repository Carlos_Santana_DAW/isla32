<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MarcaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nombre" => "required|string|max:255",
            "epigrafe" => "required",
        ];
    }

    public function messages()
    {
        return [
            "nombre.required" => "Se requiere un nombre para el productor",
            "epigrafe.required" => "Se requiere un tipo de los especificados a continuación:
            Agroalimentario,Arte,Artesanía,Deportes,Excursiones,Alquiler_vehiculo,Hospedaje_vacacional,Espectáculos o Visitas_guiadas",
        ];
    }

    public function attributes(){
        return[
            "nombre" => "Nombre del productor",
            "epigrafe" => "Tipo de marca",
        ];
    }
}
