<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;


class ClienteFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    protected $stopOnFirstFailure = true;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            "name" => "required|string|max:255",
            "email" => "required|email|max:255",
            "rol" => "required|string"
            
        ];
    }

    public function messages()
    {
        return [
            "name.required"=>"Es requerido un nombre",
            "email.required"=>"Es requerido un email",
            "rol.required"=>"Se debe especificar un rol para el usuario"
        ];
    }

    public function attributes(){
        return[
            'name'=>'nombre de usuario',
            'email'=>'email de usuario',
            'rol'=>'rol de usuario'
        ];
    }
}
