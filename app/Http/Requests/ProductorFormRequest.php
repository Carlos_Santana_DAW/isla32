<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductorFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * 'tipo' => 'required',
     */
    public function rules()
    {
        return [
            'nombre' => 'required|string|max:255',
            'direccion' => 'string|max:255',
            'correo'=>'email|max:255',
            'telefono'=>'string|max:255',
            'web'=>'string|max:255',
            'redes_sociales'=>'string|max:255',

        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Se requiere un nombre para el productor',
            'tipo.required' => 'Se requiere un tipo de los especificados a continuación:
            Agroalimentario,Arte,Artesanía,Deportes,Excursiones,Alquiler_vehiculo,Hospedaje_vacacional,Espectáculos o Visitas_guiadas',
            'direccion.required' => 'Se requiere una dirección',
            'correo.required'=>'Se requiere un correo electrónico',
            'telefono.required'=>'Se requiere un número de teléfono',
            'web.required'=>'Se requiere un sitio o dirección web',
            'redes_sociales.required'=>'Se requiere alguna red social (Ej. Twitter, Facebook, Instagram)',
        ];
    }

    public function attributes(){
        return[
            'nombre' => 'Nombre del productor',
            'tipo' => 'Tipo de actividad',
            'direccion' => 'Dirección fisica',
            'correo'=>'Correo electrónico',
            'telefono'=>'Telefono del productor',
            'web'=>'Dirección web',
            'redes_sociales'=>'redes sociales el productor',
        ];
    }
}
